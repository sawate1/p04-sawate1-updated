//
//  main.m
//  p04-sawate1
//
//  Created by Shivani Awate on 5/3/17.
//  Copyright © 2017 shivani. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
