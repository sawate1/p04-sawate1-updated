//
//  ViewController.m
//  p04-sawate1
//
//  Created by Shivani Awate on 5/3/17.
//  Copyright © 2017 shivani. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end


int integerWidth;
int    integerHeight;
int birdUp;
int RandomTopColPosition;
int RandomBottomColPosition;
int scoreNum;
int highscoreNum;


@implementation ViewController
@synthesize top, bot, colBottom, colTop;
@synthesize highScore;
@synthesize startButton;
@synthesize bird;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getScreenWidth];
    
    colTop.hidden=YES;
    colBottom.hidden=YES;
    
    
    scoreNum=0;
    highscoreNum=[[NSUserDefaults standardUserDefaults] integerForKey:@"hScore"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)startGame:(id)sender {
    startButton.hidden=YES;
    
    BirdMove=[NSTimer scheduledTimerWithTimeInterval:0.05 target:self selector:@selector(birdMoving) userInfo:nil repeats:YES];
    
    [self theCols];
    ColMove=[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(colMoving) userInfo:nil repeats:YES];
    colTop.hidden=NO;
    colBottom.hidden=NO;
    bird.center=CGPointMake(bird.center.x, 250);
    bird.hidden=NO;
    
    scoreNum=0;
    highScore.text=[NSString stringWithFormat:@"%d",scoreNum];
}
-(void)theCols{
    int num =integerHeight/2;
    RandomTopColPosition=arc4random() % num;
    RandomTopColPosition=RandomTopColPosition-228;
    
    RandomBottomColPosition=RandomTopColPosition + 655;
    
    colTop.center=CGPointMake(integerWidth+25, RandomTopColPosition);
    colBottom.center=CGPointMake(integerWidth+25, RandomBottomColPosition);
    
    
}

-(void)colMoving{
    colTop.center=CGPointMake(colTop.center.x-1, colTop.center.y);
    colBottom.center=CGPointMake(colBottom.center.x-1, colBottom.center.y);
    
    if(colBottom.center.x<-28){
        [self theCols];
    }
    
    
    if(colBottom.center.x==28){
        [self score];
    }
    
    //Look for collisions
    
    if(CGRectIntersectsRect(bird.frame, colTop.frame)){
        [self gameOver];
    }
    
    if(CGRectIntersectsRect(bird.frame, colBottom.frame)){
        [self gameOver];
    }
    
    if(CGRectIntersectsRect(bird.frame, top.frame)){
        [self gameOver];
    }
    
    if(CGRectIntersectsRect(bird.frame, bot.frame)){
        [self gameOver];
    }
    
}

-(void) score{
    scoreNum=scoreNum+1;
    highScore.text=[NSString stringWithFormat:@"%d",scoreNum];
    
    
}
-(void)gameOver{
    [ColMove invalidate];
    [BirdMove invalidate];
    colTop.hidden=YES;
    colBottom.hidden=YES;
    startButton.hidden=NO;
    bird.hidden=YES;
    
    if(scoreNum>highscoreNum){
        [[NSUserDefaults standardUserDefaults] setInteger:scoreNum forKey:@"hScore"];
    }
    
}

-(void)getScreenWidth{
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    integerWidth = (int) roundf(screenWidth);
    integerHeight = (int) roundf(screenHeight);
}

-(void) birdMoving{
    bird.center=CGPointMake(bird.center.x, bird.center.y+birdUp);
    
    
    birdUp=birdUp+5;//bird falls if you dont tap the screen
    
    if(birdUp>15){
        birdUp=15;
    }
    
    
    if(birdUp>0){
        bird.image=[UIImage imageNamed:@"birds1.png"];
    }else{
        bird.image=[UIImage imageNamed:@"birds2.png"];
    }
    
}



-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    birdUp=-30;
    
}




@end
