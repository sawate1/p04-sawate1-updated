//
//  AppDelegate.h
//  p04-sawate1
//
//  Created by Shivani Awate on 5/3/17.
//  Copyright © 2017 shivani. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

